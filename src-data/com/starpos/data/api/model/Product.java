//    GrowPOS
//    Copyright (c) 2018 GrowPOS
//    http://GrowPOS.co
//
//    This file is part of GrowPOS
//
//    GrowPOS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   GrowPOS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GrowPOS.  If not, see <http://www.gnu.org/licenses/>.

package com.starpos.data.api.model;

import com.openbravo.data.loader.Datas;
import com.openbravo.data.model.Field;
import com.openbravo.format.Formats;
import com.openbravo.pos.forms.AppLocal;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import sun.misc.BASE64Encoder;

/**
 * Last modification: 08-09-2018 9:24 pm
 * @description 
 * @author ceul
 */
public class Product {
    
    private String id;
    private String reference;
    private String code;
    private String codeType;
    private String name; 
    private double priceBuy;
    private double priceSell;
    private String category;
    private String taxCat;
    private String attributeSet_Id;
    private double stockCost;
    private double stockVolume;
    private String image;
    private boolean isCom;
    private boolean isScale;
    private boolean isConstant; 
    private boolean printkb;
    private boolean sendStatus; 
    private boolean isService;
    private byte attributes; 
    private String display;
    private boolean isVPrice; 
    private boolean isVerpatrib;
    private String textTip;
    private boolean warranty;
    private double stockUnits;
    private String printTo;
    private String supplier; 
    private String UOM;
    private boolean flag;
    private String description;
    private String short_description;
    private double weigth;
    private double width;
    private double length;
    private double height;
    private boolean isCatalog;
    private int catOrder;

    public Product() {
        
    }

    public Product(String id, String reference, String code, String codeType, String name, double priceBuy, double priceSell, String category, 
            String taxCat, String attributeSet_Id, double stockCost, double stockVolume, BufferedImage image, boolean isCom, boolean isScale, boolean isConstant,
            boolean printkb, boolean sendStatus, boolean isService, byte attributes, String display, boolean isVPrice, boolean isVerpatrib, 
            String textTip, boolean warranty, double stockUnits, String printTo, String supplier, String UOM, boolean flag, String description, 
            String short_description,double weigth,double width,double length, double height,boolean isCatalog, int catOrder) {
        this.id = id;
        this.reference = reference;
        this.code = code;
        this.codeType = codeType;
        this.name = name;
        this.priceBuy = priceBuy;
        this.priceSell = priceSell;
        this.category = category;
        this.taxCat = taxCat;
        this.attributeSet_Id = attributeSet_Id;
        this.stockCost = stockCost;
        this.stockVolume = stockVolume;
        this.image = encodeToString(image);
        this.isCom = isCom;
        this.isScale = isScale;
        this.isConstant = isConstant;
        this.printkb = printkb;
        this.sendStatus = sendStatus;
        this.isService = isService;
        this.attributes = attributes;
        this.display = display;
        this.isVPrice = isVPrice;
        this.isVerpatrib = isVerpatrib;
        this.textTip = textTip;
        this.warranty = warranty;
        this.stockUnits = stockUnits;
        this.printTo = printTo;
        this.supplier = supplier;
        this.UOM = UOM;
        this.flag = flag;
        this.description = description;
        this.short_description = short_description;
        this.weigth= weigth;
        this.width = width;
        this.length = length;
        this.height = height;
        this.isCatalog = isCatalog;
        this.catOrder = catOrder;
    }
    
    public Product (Object params ){
        try {
            Object[] paramsArray = (Object[]) params;
            this.id = (String) paramsArray[0];
            this.reference = (String) paramsArray[1];
            this.code = (String) paramsArray[2];
            this.codeType = (String) paramsArray[3];
            this.name = (String) paramsArray[4];
            this.priceBuy = (double) paramsArray[5];
            this.priceSell = (double) paramsArray[6];
            this.category = (String) paramsArray[7];
            this.taxCat = (String) paramsArray[8];
            this.attributeSet_Id = (String) paramsArray[9];
            this.stockCost = (double) paramsArray[10];
            this.stockVolume = (double) paramsArray[11];
            this.image = encodeToString((BufferedImage) paramsArray[12]);
            this.isCom = (boolean) paramsArray[13];
            this.isScale = (boolean) paramsArray[14];
            this.isConstant = (boolean) paramsArray[15];
            this.printkb = (boolean) paramsArray[16];
            this.sendStatus = (boolean) paramsArray[17];
            this.isService = (boolean) paramsArray[18];
            // this.attributes = (byte) paramsArray[19];
            this.display = (String) paramsArray[20];
            this.isVPrice = (boolean) paramsArray[21];
            this.isVerpatrib = (boolean) paramsArray[22];
            this.textTip = (String) paramsArray[23];
            this.warranty = (boolean) paramsArray[24];
            this.stockUnits = (double) paramsArray[25];
            this.printTo = (String) paramsArray[26];
            this.supplier = (String) paramsArray[27];
            this.UOM = (String) paramsArray[28];
            this.flag = (boolean) paramsArray[29];
            this.description = (String) paramsArray[30];
            this.short_description = (String) paramsArray[31];
            this.weigth= (double) paramsArray[32];
            this.width = (double) paramsArray[33];
            this.length = (double) paramsArray[34];
            this.height = (double) paramsArray[35];
            this.isCatalog = (boolean) paramsArray[36];
           // this.catOrder = (int) paramsArray[31];
        } catch (Exception e) {
            System.out.println("Error in constructor: "+ e + Product.class.getName());

        }
    }
    
    public static String encodeToString(BufferedImage image) {//, String type) {
        String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, "png", bos);
            byte[] imageBytes = bos.toByteArray();

            BASE64Encoder encoder = new BASE64Encoder();
            imageString = encoder.encode(imageBytes);

            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageString;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the codeType
     */
    public String getCodeType() {
        return codeType;
    }

    /**
     * @param codeType the codeType to set
     */
    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the priceBuy
     */
    public double getPriceBuy() {
        return priceBuy;
    }

    /**
     * @param priceBuy the priceBuy to set
     */
    public void setPriceBuy(double priceBuy) {
        this.priceBuy = priceBuy;
    }

    /**
     * @return the priceSell
     */
    public double getPriceSell() {
        return priceSell;
    }

    /**
     * @param priceSell the priceSell to set
     */
    public void setPriceSell(double priceSell) {
        this.priceSell = priceSell;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the taxCat
     */
    public String getTaxCat() {
        return taxCat;
    }

    /**
     * @param taxCat the taxCat to set
     */
    public void setTaxCat(String taxCat) {
        this.taxCat = taxCat;
    }

    /**
     * @return the attributeSet_Id
     */
    public String getAttributeSet_Id() {
        return attributeSet_Id;
    }

    /**
     * @param attributeSet_Id the attributeSet_Id to set
     */
    public void setAttributeSet_Id(String attributeSet_Id) {
        this.attributeSet_Id = attributeSet_Id;
    }

    /**
     * @return the stockCost
     */
    public double getStockCost() {
        return stockCost;
    }

    /**
     * @param stockCost the stockCost to set
     */
    public void setStockCost(double stockCost) {
        this.stockCost = stockCost;
    }

    /**
     * @return the stockVolume
     */
    public double getStockVolume() {
        return stockVolume;
    }

    /**
     * @param stockVolume the stockVolume to set
     */
    public void setStockVolume(double stockVolume) {
        this.stockVolume = stockVolume;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(BufferedImage image) {
        this.image = encodeToString(image);
    }

    /**
     * @return the isCom
     */
    public boolean isIsCom() {
        return isCom;
    }

    /**
     * @param isCom the isCom to set
     */
    public void setIsCom(boolean isCom) {
        this.isCom = isCom;
    }

    /**
     * @return the isScale
     */
    public boolean isIsScale() {
        return isScale;
    }

    /**
     * @param isScale the isScale to set
     */
    public void setIsScale(boolean isScale) {
        this.isScale = isScale;
    }

    /**
     * @return the isConstant
     */
    public boolean isIsConstant() {
        return isConstant;
    }

    /**
     * @param isConstant the isConstant to set
     */
    public void setIsConstant(boolean isConstant) {
        this.isConstant = isConstant;
    }

    /**
     * @return the printkb
     */
    public boolean isPrintkb() {
        return printkb;
    }

    /**
     * @param printkb the printkb to set
     */
    public void setPrintkb(boolean printkb) {
        this.printkb = printkb;
    }

    /**
     * @return the sendStatus
     */
    public boolean isSendStatus() {
        return sendStatus;
    }

    /**
     * @param sendStatus the sendStatus to set
     */
    public void setSendStatus(boolean sendStatus) {
        this.sendStatus = sendStatus;
    }

    /**
     * @return the isService
     */
    public boolean isIsService() {
        return isService;
    }

    /**
     * @param isService the isService to set
     */
    public void setIsService(boolean isService) {
        this.isService = isService;
    }

    /**
     * @return the attributes
     */
    public byte getAttributes() {
        return attributes;
    }

    /**
     * @param attributes the attributes to set
     */
    public void setAttributes(byte attributes) {
        this.attributes = attributes;
    }

    /**
     * @return the display
     */
    public String getDisplay() {
        return display;
    }

    /**
     * @param display the display to set
     */
    public void setDisplay(String display) {
        this.display = display;
    }

    /**
     * @return the isVPrice
     */
    public boolean isIsVPrice() {
        return isVPrice;
    }

    /**
     * @param isVPrice the isVPrice to set
     */
    public void setIsVPrice(boolean isVPrice) {
        this.isVPrice = isVPrice;
    }

    /**
     * @return the isVerpatrib
     */
    public boolean isIsVerpatrib() {
        return isVerpatrib;
    }

    /**
     * @param isVerpatrib the isVerpatrib to set
     */
    public void setIsVerpatrib(boolean isVerpatrib) {
        this.isVerpatrib = isVerpatrib;
    }

    /**
     * @return the textTip
     */
    public String getTextTip() {
        return textTip;
    }

    /**
     * @param textTip the textTip to set
     */
    public void setTextTip(String textTip) {
        this.textTip = textTip;
    }

    /**
     * @return the warranty
     */
    public boolean isWarranty() {
        return warranty;
    }

    /**
     * @param warranty the warranty to set
     */
    public void setWarranty(boolean warranty) {
        this.warranty = warranty;
    }

    /**
     * @return the stockUnits
     */
    public double getStockUnits() {
        return stockUnits;
    }

    /**
     * @param stockUnits the stockUnits to set
     */
    public void setStockUnits(double stockUnits) {
        this.stockUnits = stockUnits;
    }

    /**
     * @return the printTo
     */
    public String getPrintTo() {
        return printTo;
    }

    /**
     * @param printTo the printTo to set
     */
    public void setPrintTo(String printTo) {
        this.printTo = printTo;
    }

    /**
     * @return the supplier
     */
    public String getSupplier() {
        return supplier;
    }

    /**
     * @param supplier the supplier to set
     */
    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    /**
     * @return the UOM
     */
    public String getUOM() {
        return UOM;
    }

    /**
     * @param UOM the UOM to set
     */
    public void setUOM(String UOM) {
        this.UOM = UOM;
    }

    /**
     * @return the flag
     */
    public boolean isFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    /**
     * @return the isCatalog
     */
    public boolean isIsCatalog() {
        return isCatalog;
    }

    /**
     * @param isCatalog the isCatalog to set
     */
    public void setIsCatalog(boolean isCatalog) {
        this.isCatalog = isCatalog;
    }

    /**
     * @return the catOrder
     */
    public int getCatOrder() {
        return catOrder;
    }

    /**
     * @param catOrder the catOrder to set
     */
    public void setCatOrder(int catOrder) {
        this.catOrder = catOrder;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the short_description
     */
    public String getShort_description() {
        return short_description;
    }

    /**
     * @param short_description the short_description to set
     */
    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    /**
     * @return the weigth
     */
    public double getWeigth() {
        return weigth;
    }

    /**
     * @param weigth the weigth to set
     */
    public void setWeigth(double weigth) {
        this.weigth = weigth;
    }

    /**
     * @return the width
     */
    public double getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(double width) {
        this.width = width;
    }

    /**
     * @return the length
     */
    public double getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(double length) {
        this.length = length;
    }

    /**
     * @return the height
     */
    public double getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(double height) {
        this.height = height;
    }
    
    
    

}
