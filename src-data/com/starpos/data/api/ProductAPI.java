//   GrowPOS
//    Copyright (c) 2018 GrowPOS & previous Unicenta OPos works
//    http://GrowPOS.co
//
//    This file is part of GrowPOS
//
//    GrowPOS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   GrowPOS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GrowPOS.  If not, see <http://www.gnu.org/licenses/>.
package com.starpos.data.api;

import com.openbravo.basic.BasicException;
import com.openbravo.data.loader.Http;
import com.openbravo.data.loader.HttpConection;

/**
 * Last modification: 08-08-2018 12:31 am
 * @description 
 * @author ceul
 **/
public class ProductAPI implements ProductAPIInterface {
    private Http con;
    private String resource;
    private String url;
    
    public ProductAPI(){
        setUrl("http://localhost:3000");
        this.resource = null;
        this.con = new HttpConection();
    }
    
    public String createProduct(String product){
        try {
            this.con.post(getUrl()+"/product/create", product);
            return resource;
        } catch (Exception e) {
            return resource;
        }
    }
    
    public String updateProduct(String product){
        try {
            this.con.post(getUrl()+"/product/update", product);
            return resource;
        } catch (Exception e) {
            return resource;
        }
    }
    
    public String deleteProduct(String product){
        try {
            this.con.post(getUrl()+"/product/delete", product);
            return resource;
        } catch (Exception e) {
            return resource;
        }
    }
    
    public String updateStockProduct(String product){
        try {
            this.con.post(getUrl()+"/stock/update", product);
            return resource;
        } catch (Exception e) {
            return resource;
        }
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
