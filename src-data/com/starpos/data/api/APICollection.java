//    GrowPOS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   GrowPOS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GrowPOS.  If not, see <http://www.gnu.org/licenses/>.
package com.starpos.data.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.openbravo.basic.BasicException;
import com.starpos.data.api.model.Product;

/**
 * Last modification: 08-08-2018 12:31 am
 * @description This is the colletion of
 * api interfaces, if you want to consume the api
 * you wanna see the method in this class
 * @author ceul
 */
public class APICollection implements API_interface{

    private ProductAPIInterface productAPI;
    private Product product;

    public APICollection() {
        productAPI = new ProductAPI();
    }
    
    
    
    public String createProduct(Object params) throws BasicException{
        this.product = new Product(params);
        GsonBuilder builder = new GsonBuilder(); 
        builder.setPrettyPrinting(); 
        Gson gson = builder.create();
        return productAPI.createProduct(gson.toJson(this.product));
    }
    
    public String updateProduct(Object params) throws BasicException{
        this.product = new Product(params);
        GsonBuilder builder = new GsonBuilder(); 
        builder.setPrettyPrinting(); 
        Gson gson = builder.create();
        return productAPI.updateProduct(gson.toJson(product));
    }
    
    public String deleteProduct(Object params) throws BasicException{
        this.product = new Product(params);
        GsonBuilder builder = new GsonBuilder(); 
        builder.setPrettyPrinting(); 
        Gson gson = builder.create();
        return productAPI.deleteProduct(gson.toJson(product));
    }
    
    public String updateStockProduct(String id, Double units) throws BasicException{//,Object params) throws BasicException{
        this.product = new Product();
        //Object[] values = (Object[]) params;
        //this.product.setId(values[4].toString());
        //this.product.setStockUnits(Double.parseDouble(values[6].toString()));
        this.product.setId(id);
        this.product.setStockUnits(units);
        GsonBuilder builder = new GsonBuilder(); 
        builder.setPrettyPrinting(); 
        Gson gson = builder.create();
        return productAPI.updateStockProduct(gson.toJson(product));
    }
}
