//    GrowPOS
//    Copyright (c) 2018 GrowPOS
//    http://GrowPOS.co
//
//    This file is part of GrowPOS
//
//    GrowPOS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   GrowPOS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GrowPOS.  If not, see <http://www.gnu.org/licenses/>.
package com.starpos.data.api;

import com.openbravo.basic.BasicException;

/**
 * Last modification: 08-08-2018 11:21 am
 * @description This is the interface to connect 
 * to all Product API class
 * @author ceul
 */
public interface ProductAPIInterface {
    // ----------------------- Product API Secction ---------------------- //
    
    public String createProduct(String product) throws BasicException;
    public String updateProduct(String product) throws BasicException;
    public String deleteProduct(String product) throws BasicException;
    public String getUrl() throws BasicException;
    public void setUrl(String url) throws BasicException;
    public String updateStockProduct(String product);
}
