//   GrowPOS
//    Copyright (c) 2018 GrowPOS & previous Unicenta OPos works
//    http://GrowPOS.co
//
//    This file is part of GrowPOS
//
//    GrowPOS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   GrowPOS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GrowPOS.  If not, see <http://www.gnu.org/licenses/>.

package com.openbravo.data.loader;


import javax.swing.text.html.parser.Entity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 * Last modification: 08-08-2018 12:31 am
 * @description 
 * 
 * @author ceul
 */
public class HttpConection implements Http{
    private HttpClient httpClient;
    private HttpPost post;
    private HttpResponse response;
    private String resource;
    
    public HttpConection(){
        this.httpClient = HttpClients.createDefault(); //HttpClientBuilder.create().build(); //
        this.post = null;
        this.resource = null;
    }
    
    /* public String GET(String url){
        this.get = new HttpGet(url);
        try {
            this. response = this.httpClient.execute(this.get);
            this.resource =  EntityUtils.toString(this.response.getEntity());
        } catch (Exception e) {
            System.out.println("Error"+ e.getMessage());
        }
        return this.resource;
    } */
    
    public String post (String url, String body) {
        try {
            this.post = new HttpPost(url);
            StringEntity entity = new StringEntity(body, "UTF-8");
            entity.setContentType("application/json");
            this.post.setEntity(entity);
            this.response = this.httpClient.execute(this.post);
            this.resource =  EntityUtils.toString(this.response.getEntity());
            return this.resource;
        } catch (Exception e) {
            System.out.println("Error"+ e.getMessage());
            return this.resource;
        }
    }
}