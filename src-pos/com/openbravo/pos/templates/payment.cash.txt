//    uniCenta oPOS - Touch Friendly Point Of Sale
//    Copyright (c) 2009-2016 uniCenta
//    http://sourceforge.net/projects/unicentaopos
//
//    This file is part of uniCenta oPOS.
//
//    uniCenta oPOS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    uniCenta oPOS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with uniCenta oPOS.  If not, see <http://www.gnu.org/licenses/>.
// *************************************************************************

payment.addButton("billete-1000", 1000.0);
payment.addButton("billete-2000", 2000.0);
payment.addButton("billete-5000", 5000.0);
payment.addButton("billete-10000", 10000.0);
payment.addButton("billete-20000", 20000.0);
payment.addButton("billete-50000", 50000.0);
payment.addButton("billete-100000", 100000.0);
payment.addButton("moneda-100", 100.0);
payment.addButton("moneda-200", 200.0);
payment.addButton("moneda-500", 500.0);
payment.addButton("moneda-1000", 1000.0);
payment.addButton("moneda-50", 50.0);